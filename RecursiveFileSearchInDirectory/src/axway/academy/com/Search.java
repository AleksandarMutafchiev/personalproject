package axway.academy.com;

import java.io.File;

public class Search extends Thread{

	
	private String name;
	
	public Search(String name) {
		super();
		this.name = name;
	}
	
	public void bringAll(String path) {

		File dir = new File(path);
		if (!dir.isDirectory()) {
			System.out.println("Is not a directory");
			return;
		}
		for (File f : dir.listFiles()) {
			if(f.isFile()) {
				if(f.getName().endsWith(".txt")) {
				System.out.println(f.getAbsolutePath());
				}
			}
			else {
				bringAll(f.getAbsolutePath());
			}
		}
	}
	@Override
	public void run() {
		bringAll("C:\\Users\\A. Mutafchiev\\Desktop\\files");
	}
	
}
